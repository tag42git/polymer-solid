# polymer-solid
 Please Note: this has not been tested with recent versions of node-solid-server

Polymer-Solid is a collection of rudimentary html elements for use in composing clients for [Solid](https://github.com/solid) servers, which are [Linked Data Platform]( https://en.m.wikipedia.org/wiki/Linked_Data_Platform) compliant servers with [Web Access Control](https://www.w3.org/wiki/WebAccessControl) for authorization.

I would like to use [web components](https://en.wikipedia.org/wiki/Web_Components) as building blocks for the front end of an application, and then use the web itself for the database.  &nbsp; Instead of a silo.

There are a lot of pure Javascript components (with better provenance) available from the Solid team, but my goal with polymer-solid is to be able to compose apps with HTML and use templates.  &nbsp;I avoid CSS with these elements to try to keep them simpler, so unfortunately it has a gray boring look so far.  &nbsp;I also try to avoid third party libraries and custom elements from the Polymer catalog to try to keep it open, in anticipation of a move away from Polymer to pure W3C web components.  &nbsp;Of course lots of new functionality is needed.   

To run this locally, clone or download the repo and you'll need some dependencies.  &nbsp;This uses [rdflib.js](https://github.com/linkeddata/rdflib.js/) for [rdf](https://en.wikipedia.org/wiki/Resource_Description_Framework) serialization/deserialization.  &nbsp;[Solid-auth-client](https://github.com/solid/solid-auth-client) is used for authorization and requests to the server.  &nbsp;You'll also need the [Polymer command line utility](https://github.com/polymer/polymer-cli).


npm install -g polymer-cli
<br>
git clone https://gitlab.com/tag42git/polymer-solid.git
<br>
cd polymer-solid
<br>
polymer install
<br>
npm install
<br>
polymer serve --verbose
<br>

Then you can access it at http://localhost:8081.

To build and run a static version (if you don't have http-server, npm install -g http-server):

polymer build
<br>
cp popup.html build/default/.
<br>
cd build/default
<br>
http-server
<br>

and you can access that at http://localhost:8080

Sorry, but its only been tested on recent versions of Chrome and Chromium.



