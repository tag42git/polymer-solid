
var languages = (function () {
	var o = {};

  o.getLanguages = function() {
    return [
            {val:'en',label:'English / 英语'},
            {val:'zh',label:'Chinese / 中文'}
            ];
  }

  o.moreStrs = null;

  o.moreStrings = function(strings) {
    o.moreStrs = strings;
  }

  o.getString = function(lang,keyStr) {
     if (strs[keyStr] === undefined) {
        //console.log("can't find keyStr= "+keyStr+", looking further");
        if (o.moreStrs == null || o.moreStrs[keyStr] === undefined)
          console.log("can't find keyStr= "+keyStr);
        else
          return o.moreStrs[keyStr][lang];
      }

      return strs[keyStr][lang];
	};

  const strs =
  {
    "Display language":
      {
        en:"Display language",
        zh:"显示语言"
      },
    "polymer-solid-demo":
      {
        en:"polymer-solid-demo",
        zh:"聚合物固体演示"
      },
    "Hide Settings":
      {
        en:"Hide Settings",
        zh:"主屏幕"
      },
    "Settings":
      {
        en:"Settings / 设置",
        zh:"设置"
      },
    "Hide Directories":
      {
        en:"Hide Directories",
        zh:"隐藏目录"
      },
    "Show Directories":
      {
        en:"Show Directories",
        zh:"显示目录"
      },
    "Hide Containers":
      {
        en:"Hide Containers",
        zh:"隐藏容器"
      },
    "Show Containers":
      {
        en:"Show Containers",
        zh:"显示容器"
      },
    "Hide ACL Editor":
      {
        en:"Hide ACL Editor",
        zh:"隐藏ACL编辑器"
      },
    "Show ACL Editor":
      {
        en:"Show ACL Editor",
        zh:"显示ACL编辑器"
      },
    "Solid Server":
      {
        en:"Solid Server:",
        zh:"固体 服务器:"
      },
    "Users Web ID:":
      {
        en:"Users Web ID:",
        zh:"用户 卷筒纸 ID:"
      },
    "Login":
      {
        en:"Login",
        zh:"登录"
      },
    "Sign Up":
      {
        en:"Sign Up",
        zh:"注册"
      },
    "Solid Resource":
      {
        en:"Solid Resource",
        zh:"固体资源"
      },
    "Resource":
      {
        en:"Resource",
        zh:"资源"
      },
    "new container":
      {
        en:"new container",
        zh:"新集装箱"
      },
    "new resource":
      {
        en:"new resource",
        zh:"新资源"
      },
    "save resource":
      {
        en:"save resource",
        zh:"节省资源"
      },
    "Save Resource as":
      {
        en:"Save Resource as",
        zh:"将资源保存为"
      },
    "Create New Container":
      {
        en:"Create New Container",
        zh:"创建新容器"
      },
    "Create New Resource":
      {
        en:"Create New Resource",
        zh:"创建新资源"
      },
    "Create":
      {
        en:"Create",
        zh:"创建"
      },
    "Cancel":
      {
        en:"Cancel",
        zh:"取消"
      },
    "Name":
      {
        en:"Name",
        zh:"名称"
      },
    "Size":
      {
        en:"Size",
        zh:"尺寸"
      },
    "Modified":
      {
        en:"Modified",
        zh:"改性"
      },
    "Delete":
      {
        en:"Delete",
        zh:"删除"
      },
    "up":
      {
        en:"up",
        zh:"向上"
      },
    "Edit":
      {
        en:"Edit",
        zh:"编辑"
      },
    "View":
      {
        en:"View",
        zh:"视图"
      },
    "Are you sure you want to delete ":
      {
        en:"Are you sure you want to delete ",
        zh:"你确定你要删除 "
      },
    "Are you sure you want to discard changes":
      {
        en:"Are you sure you want to discard changes",
        zh:"你确定要放弃更改吗"      
      },
    "Successfully deleted ":
      {
        en:"Successfully deleted ",
        zh:"成功删除 "
      },
    "Sorry, but right now you must create containers/resources only in the current container.":
      {
        en:"Sorry, but right now you must create containers/resources only in the current container.",
        zh:"对不起，但现在您只能在当前容器中创建容器/资源。"
      },
     "Sorry, but right now '\\' is not permitted in container or resource names.":
      {
        en:"Sorry, but right now '\\' is not permitted in container or resource names.",
        zh:"对不起，但是现在不允许在容器或资源名称中使用'\\'。"
      },
    "Are you sure you want to create ":
      {
        en:"Are you sure you want to create ",
        zh:"您确定要创建吗 "
      },
    "Do you want to create ":
      {
        en:"Do you want to create ",
        zh:"你想创建吗 "
      },
    "Are you sure you want to save as ":
      {
        en:"Are you sure you want to save as ",
        zh:"你确定要保存吗 "
      },
    "successfully read ":
      {
        en:"successfully read ",
        zh:"成功阅读 "
      },
    "successfully saved ":
      {
        en:"successfully saved ",
        zh:"成功保存 "
      },
    "can't read ":
      {
        en:"can't read ",
        zh:"看不懂 "
      },
    " because: ":
      {
        en:" because: ",
        zh:"因为："
      },
    "access":
      {
        en:"access",
        zh:"访问"
      },
    "failed:":
      {
        en:"failed:",
        zh:"失败:"
      },
    "Can't ":
      {
        en:"Can't ",
        zh:"不能 "
      },
    " resource, reason: ":
      {
        en:" resource, reason: ",
        zh:"资源，原因: "
      },
    "successfully":
      {
        en:"successfully",
        zh:"顺利"
      },
    "put":
      {
        en:"put",
        zh:"放"
      },
    "patched":
      {
        en:"patched",
        zh:"修补"
      },
    "patch":
      {
        en:"patch",
        zh:"补丁"
      },
    "created":
      {
        en:"created",
        zh:"创建"
      },
    "logged in":
      {
        en:"logged in",
        zh:"登录"
      },
    "couldn't get session":
      {
        en:"couldn't get session",
        zh:"无法获得会话"
      },
    "couldn't authorize, reason":
      {
        en:"couldn't authorize, reason",
        zh:"无法授权，理由"
      },
    "unable to authorize, error":
      {
        en:"unable to authorize, error",
        zh:"无法授权，错误"
      },
    "Save":
      {
        en:"Save",
        zh:"保存"
      },
    "Discard changes":
      {
        en:"Discard changes",
        zh:"放弃更改"
      },
    "Can't edit container metadata":
      {
        en:"Can't edit container metadata",
        zh:"无法编辑容器元数据"
      },
    "No write permission":
      {
        en:"No write permission",
        zh:"没有写许可"
      },
    "can't get WAC-Allow header, because:":
      {
        en:"can't get WAC-Allow header, because:",
        zh:""
      },
    "Can't get WAC-Allow, reason:":
      {
        en:"Can't get WAC-Allow, reason:",
        zh:"无法获取WAC-Allow标头，因为:"
      },
    "Can't get Web Access Control (WAC) header":
      {
        en:"Can't get Web Access Control (WAC) header",
        zh:"无法获取Web访问控制（WAC）标头"
      },
    "Are you sure you want to overwrite":
      {
        en:"Are you sure you want to overwrite",
        zh:"你确定要覆盖吗？"
      },
    "Unable to save":
      {
        en:"Unable to save",
        zh:"无法保存"
      },
    "because its not valid turtle format":
      {
        en:"because its not valid turtle format",
        zh:"因为它无效的乌龟格式"
      },
    "because its not valid RDF format":
      {
        en:"because its not valid RDF format",
        zh:"因为它不是有效的RDF格式"
      },
    "format":
      {
        en:"format",
        zh:"格式"
      },
    "because it's not valid":
      {
        en:"because it's not valid",
        zh:"因为它无效"
      },
    "Save as":
      {
        en:"Save as",
        zh:"另存为"
      },
    "Change Server":
      {
        en:"Change Server",
        zh:"更改服务器"
      },
    "can't get ACL url for ":
      {
        en:"can't get ACL url for ",
        zh:"无法获取ACL网址 "
      },
    "can't get ACL url because ":
      {
        en:"can't get ACL url because ",
        zh:"无法获取ACL网址 "
      },
    "can't get ACL url, reason: ":
      {
        en:"can't get ACL url, reason: ",
        zh:"无法获取ACL网址，原因: "
      },
    "Invalid resource name":
      {
        en:"Invalid resource name",
        zh:"资源名称无效"
      },
    "Sorry, but right now you can't create container with 'save as'":
      {
        en:"Sorry, but right now you can't create container with 'save as'",
        zh:"对不起，但现在你不能用“另存为”创建容器"
      },
    "The resource already exists":
      {
        en:"The resource already exists",
        zh:"资源已经存在"
      },
    "Please enter the path to your container":
      {
        en:"Please enter the path to your container",
        zh:"请输入您的容器的路径"
      },
    "Sorry, but this is an invalid container or resource name.":
      {
        en:"Sorry, but this is an invalid container or resource name.",
        zh:"对不起，但这是一个无效的容器或资源名称"
      },
    "New":
      {
        en:"New",
        zh:"新"
      },
    "Ok":
      {
        en:"Ok",
        zh:"好"
      },
    "Helpful links":
      {
        en:"Helpful links",
        zh:"有用的网址"
      },
    "resources":
      {
        en:"resources",
        zh:"资源"
      },
    "subjects for":
      {
        en:"subjects for",
        zh:"科目"
      },
    "not implemented yet":
      {
        en:"not implemented yet",
        zh:"尚未实施"
      },
    "source at":
      {
        en:"source at",
        zh:"来源于"
      }

  }

	return o;
}());

